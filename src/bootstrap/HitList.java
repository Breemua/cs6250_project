package bootstrap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

import common.Log;

/**
 * This class contains the 200 most recently used IPs of peers as well as IP
 * subnets where the probability to hit a peer is high.
 * 
 */
public class HitList {

	/**
	 * List of IPs that were recently encountered in the network.
	 */
	private static Vector<String> IPs = new Vector<String>();

	/**
	 * List of all ever encountered subnets.
	 */
	private static Vector<Subnet> subnets = new Vector<Subnet>();

	/**
	 * hitlist
	 */
	private static HitList hitlist;

	/**
	 * Fetches data from the file List.txt
	 */
	public HitList() {
		try {
			FileInputStream fis = new FileInputStream("src/List.txt");
			DataInputStream in = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				if (isIP(strLine)) {
					IPs.add(strLine);
				} else if (isSubnet(strLine)) {
					String split[] = strLine.split(",");
					subnets.add(new Subnet(split[0], Float.parseFloat(split[1])));
				} else if (!strLine.equals("")) {
					Log.getLogger().warning(
							"This input of the HitList is obviously bad : "
									+ strLine);
				}
			}
			in.close();

		} catch (FileNotFoundException e) {
			Log.getLogger().info("ERROR : Bootstrapping list not found.");
		} catch (IOException e) {
			Log.getLogger().info("ERROR : Bootstrapping list cannot be read.");
		}
		hitlist = this;
	}

	private boolean isSubnet(String string) {
		boolean res = false;
		String inet[] = null;
		String split[] = string.split(",");
		if (split.length == 2) {
			inet = split[0].split("\\.");
			if (inet.length == 4) {
				res = true;
			}
		}
		if (res) {
			try {
				int num;
				for (int i = 0; i < 4; i++) {
					num = Integer.parseInt(inet[i]);
					if (num < 0 || num > 255) {
						Log.getLogger()
								.warning(
										"This input of the HitList is bad : "
												+ string
												+ "\nNumbers should be between 0 and 255");
						res = false;
						continue;
					}
				}
			} catch (NumberFormatException e) {
				Log.getLogger().warning(
						"This input of the HitList is bad : " + string
								+ "\nThe IP address should contain numbers!");
				res = false;
			}
		}
		return res;
	}

	private boolean isIP(String string) {
		boolean res = false;
		String inet[] = null;
		String split[] = string.split(",");
		if (split.length == 1) {
			inet = split[0].split("\\.");
			if (inet.length == 4) {
				res = true;
				split = inet[3].split(":");
				res = true;
				if (split.length != 2) {
					Log.getLogger().warning(
							"This is a bad input in the HitList : " + string
									+ "\nIt should be IP:port or IP,hitrate");
					res = false;
				}
			}
		}
		if (res) {
			try {
				int num;
				inet[3] = split[0];
				for (int i = 0; i < 4; i++) {
					num = Integer.parseInt(inet[i]);
					if (num < 0 || num > 255) {
						Log.getLogger()
								.warning(
										"This input of the HitList is bad : "
												+ string
												+ "\nNumbers should be between 0 and 255");
						res = false;
						continue;
					}
				}
			} catch (NumberFormatException e) {
				Log.getLogger().warning(
						"This input of the HitList is bad : " + string
								+ "\nThe IP address should contain numbers!");
			}
		}
		return res;
	}

	/**
	 * Adds the IP address at the top of the hitlist for next bootstrap.
	 * 
	 * @param IP
	 *            IP address to add.
	 */
	public static boolean addIP(String IP, int port) {
		int i = 0;
		boolean stop = false;
		if (IPs.contains(IP + ":" + port)) {
			IPs.remove(IP + ":" + port);
			IPs.add(0, IP + ":" + port);
			return false;
		}
		IPs.add(0, IP + ":" + port);
		while (!stop && i < subnets.size()) {
			stop = subnets.get(i).update(IP);
			i++;
		}
		// update position of the updated subnet according to hit rate
		if (stop) {
			i--;
			stop = false;
			int j = 0;
			while (j < subnets.size() && !stop) {
				if (subnets.get(j).getHitRate() <= subnets.get(i).getHitRate()) {
					stop = true;
				}
				j++;
			}
			subnets.add(j - 1, subnets.get(i));
			subnets.remove(i + 1);
		} else if (!stop && !(Subnet.isClass(IP) == Subnet.DorE)) {
			i = 0;
			stop = false;
			Subnet sub = new Subnet(IP);
			while (i < subnets.size() && !stop) {
				if (subnets.get(i).getHitRate() < sub.getHitRate()) {
					stop = true;
				}
				i++;
			}
			subnets.add(i, sub);
		}
		return true;
	}

	/**
	 * Saves the hitList in the dedicated file.
	 */
	public static void save() {
		try {
			FileWriter fstream = new FileWriter("src/List.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for (String ip : IPs) {
				out.write(ip + "\n");
			}
			/*
			 * for (i = IPs.size(); i < 199; i++) { out.write("\n"); }
			 */
			for (Subnet s : subnets) {
				out.write(s.print());
			}
			out.close();

		} catch (FileNotFoundException e) {
			Log.getLogger().info("ERROR : Bootstrapping list not found.");
		} catch (IOException e) {
			Log.getLogger().info("ERROR : Bootstrapping list cannot be read.");
		}
	}

	/**
	 * Getter of recent IPs
	 * 
	 * @return IPs
	 */
	public static Vector<String> getIPs() {
		return IPs;
	}

	/**
	 * Getter of subnets
	 * 
	 * @return subnets
	 */
	public static Vector<Subnet> getSubnets() {
		return subnets;
	}

	/**
	 * Returns the current IPs and subnets in a string format
	 */
	public String toString() {
		String s = "";
		for (String st : IPs) {
			s += st + "\n";
		}
		for (Subnet sub : subnets) {
			s += sub.toString() + "\n";
		}
		return s;
	}

	/**
	 * Getter of highest hit rate subnet
	 * 
	 * @return subnet with highest hit rate
	 */
	public Subnet getBestSubnet() {
		Subnet sub = null;
		try {
			sub = subnets.get(0);
		} catch (ArrayIndexOutOfBoundsException e) {
			Log.getLogger().warning("No Subnet specified in the HitList");
		}
		return sub;
	}

	/**
	 * Decrease the hit rate of the subnet with highest throuphput by a factor
	 * of two.
	 */
	public void decreaseHitRate() {
		Subnet s = getBestSubnet();
		s.decreaseHitRate();
		if (s.getHitRate() < subnets.get(1).getHitRate()) {
			boolean stop = false;
			int i = 1;
			subnets.remove(0);
			while (!stop && i < subnets.size()
					&& s.getHitRate() < subnets.get(i).getHitRate()) {
				i++;
			}
			subnets.add(i, s);
			stop = true;
		}
	}

	public static void exit() {
		Log.getLogger().info(hitlist.toString());
		save();
	}

}
