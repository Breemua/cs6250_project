package bootstrap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;

import chord.ChordNode;

import messages.JoinMessage;
import messages.ListMessage;
import messages.Message;

import common.Log;

public class Bootstrapper implements Runnable {

	private HitList hitList;
	static private Vector<String> aliveNodes;
	private final int defPort = 5632;
	private final int nbAlive = 4;
	private final int probeTimeout = 200;
	private ChordNode chordNode;
	private final int nbProbes = 500;
	private long timer;

	/**
	 * Initializes the hitList.
	 */
	public Bootstrapper() {
		hitList = new HitList();
		aliveNodes = new Vector<String>();
	}

	/**
	 * Connects to other nodes in the network
	 */
	@Override
	public void run() {
		synchronized (this) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		findNodes();
		HitList.save();
	}

	private void findNodes() {
		timer = System.currentTimeMillis();
		String tmp[];
		int i = 0;

		int ctr = 0;
		Vector<String> IPs = HitList.getIPs();
		Subnet subnet = hitList.getBestSubnet();

		while (aliveNodes.size() < 10 && IPs.size() > i) {
			tmp = IPs.get(i).split(":");
			i++;
			checkNode(tmp[0], Integer.parseInt(tmp[1]));
		}

		i = 0;
		// stop after 500 attempts
		if (subnet != null) {
			while (aliveNodes.size() < nbAlive && ctr < nbProbes) {
				checkNode(subnet.getRandomIP(), defPort);
				i++;
				ctr++;
				// Log.getLogger().info(2*1/(subnet.getHitRate()));
				if (i > 2 * 1 / (subnet.getHitRate())) {
					//Log.getLogger().info("next subnet");
					hitList.decreaseHitRate();
					subnet = hitList.getBestSubnet();
					i = 0;
					//Log.getLogger().info("changed");
				}
			}
		}
		if (nbAlive > aliveNodes.size()) {
			Log.getLogger().info("Only found " + aliveNodes.size() + " node(s).");
		} else {
			Log.getLogger().info("Bootstrap successful." + aliveNodes);
		}

	}

	private boolean ownIP(String ipAddr) {
		boolean res = false;

		try {
			Enumeration<NetworkInterface> e = NetworkInterface
					.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface n = e.nextElement();
				Enumeration<InetAddress> ee = n.getInetAddresses();
				while (ee.hasMoreElements()) {
					InetAddress i = ee.nextElement();
					if (i.getHostAddress().toString().contains(ipAddr)) {
						res = true;
					}
				}
			}

		} catch (SocketException e) {
			e.printStackTrace();
		}

		return res;
	}

	public void checkNode(String ip, int port) {
		if (ownIP(ip)) {
			return;
		}
		Socket socket;
		ObjectOutputStream oos;
		ObjectInputStream ois;
		Message msg;
		//Log.getLogger().info("Trying to connect to : " + ip + ":" + port);
		try {
			// socket = new Socket("127.0.0.1", 5632);
			socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port), probeTimeout);

			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());

			msg = new JoinMessage(null);

			oos.writeObject(msg);

			msg = (Message) ois.readObject();

			if (msg instanceof ListMessage) {
				Log.getLogger().info("ListMsg received from " + ip);
			}
			Vector<String> nodes = ((ListMessage) msg).getIPs();
			aliveNodes.add(ip + ":" + port);
			HitList.addIP(ip, port);
			checkAliveNodes(nodes);
			oos.close();
			ois.close();
			socket.close();
			System.out.println("\n\n\nBOOOOOOOOT"+(System.currentTimeMillis()-timer));
			synchronized (chordNode) {
				chordNode.notify();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			//Log.getLogger().info("Connection refused, moving on.");
		} catch (ClassNotFoundException e) {
			// Log.getLogger().info("3");
			e.printStackTrace();
		}
	}

	private void checkAliveNodes(Vector<String> nodes) {
		String tmp[];
		for (String s : nodes) {
			if (aliveNodes.size() < nbAlive && !aliveNodes.contains(s)) {
				System.out.println(s);
				tmp = s.split(":");
				checkNode(tmp[0], Integer.parseInt(tmp[1]));
			}
		}
	}

	public static Vector<String> getAliveNodes() {
		return aliveNodes;
	}

	public static String getFirstNode() {
		String[] tmp = aliveNodes.get(0).split(":");
		return tmp[0];
	}

	public void setChord(ChordNode node) {
		chordNode = node;

		synchronized (this) {
			this.notify();
		}
	}

}
