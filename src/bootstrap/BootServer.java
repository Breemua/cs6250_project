package bootstrap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

import messages.JoinMessage;
import messages.ListMessage;
import messages.Message;

import common.Log;

public class BootServer implements Runnable {

	private Socket fromClientSocket;

	@Override
	public void run() {

		int port = 5632;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			Log.getLogger().info("Waiting for a connection on " + port);

			while (true) {
				fromClientSocket = serverSocket.accept();
				ObjectOutputStream oos = new ObjectOutputStream(
						fromClientSocket.getOutputStream());

				ObjectInputStream ois = new ObjectInputStream(
						fromClientSocket.getInputStream());

				Message msg = (Message) ois.readObject();

				if (msg instanceof JoinMessage) {
					Log.getLogger().info(
							"JoinMsg received from "
									+ fromClientSocket.getInetAddress()
											.getHostAddress());
					oos.writeObject(new ListMessage("", Bootstrapper
							.getAliveNodes()));
					HitList.addIP(fromClientSocket.getLocalAddress()
							.getHostAddress(), port);
				}

				oos.close();
				ois.close();
				fromClientSocket.close();
			}
		} catch (BindException e1) {
			Log.getLogger().severe("Port " + port + " already in use");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
