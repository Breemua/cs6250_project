package bootstrap;

import java.util.Random;

import common.Log;

/**
 * 
 * This class hold
 * 
 */
public class Subnet {

	static final int A = 16777214;
	static final int B = 65534;
	static final int C = 254;
	static final int DorE = 1;
	private float hit_rate;
	private String IP;
	private int size;

	public Subnet(String s, float hitRate) {
		IP = s;
		hit_rate = hitRate;
		int ipClass = isClass(s);
		if (ipClass != DorE) {
			size = ipClass;
		} else {
			Log.getLogger().info("ERROR : Subnet of class D or E unhandled");
		}
		return;
	}

	public Subnet(String s) {
		IP = s;
		int ipClass = isClass(s);
		if (ipClass != DorE) {
			size = ipClass;
		} else {
			Log.getLogger().info("ERROR : Subnet of class D or E unhandled");
			return;
		}
		hit_rate = (float) 1 / size;
	}

	/**
	 * Updates the hit_rate if the IP address matches this subnet.
	 * 
	 * @param IP
	 * @return true if the IP address is in the subnet
	 */
	public boolean update(String IP) {
		boolean res = false;
		if (this.contains(IP)) {
			hit_rate += (float) 1 / size;
			res = true;
		}
		return res;
	}

	/**
	 * 
	 * @param ip
	 * @return true if the IP address is in the subnet
	 */
	private boolean contains(String ip) {
		boolean result = false;
		String bytes[] = ip.split("\\.");
		String ownbytes[] = IP.split("\\.");
		switch (size) {
		case A:
			result = bytes[0].equals(ownbytes[0]);
			break;

		case B:
			result = bytes[0].equals(ownbytes[0])
					&& bytes[1].equals(ownbytes[1]);
			break;

		case C:
			result = bytes[0].equals(ownbytes[0])
					&& bytes[1].equals(ownbytes[1])
					&& bytes[2].equals(ownbytes[2]);
			break;
		}
		return result;
	}

	public float getHitRate() {
		return this.hit_rate;
	}

	public static int isClass(String address) {
		String bytes[] = address.split("\\.");
		int a = Integer.parseInt(bytes[0]);
		int result;
		/* If class A */
		if (a < 128) {
			result = A;
		}
		/* class B */
		else if (a < 192) {
			result = B;
		}
		/* class C */
		else if (a < 224) {
			result = C;
		}
		/* class D or E */
		else {
			result = DorE;
		}
		return result;
	}

	public String print() {
		return IP + "," + hit_rate + "\n";
	}

	public String toString() {
		String s = IP;
		switch (size) {
		case A:
			s += "\\8";
			break;

		case B:
			s += "\\16";
			break;

		case C:
			s += "\\24";
			break;
		}
		return s + " " + hit_rate;
	}

	public int getSize() {
		return this.size;
	}

	public String getRandomIP() {
		String result = "";
		String bytes[] = this.IP.split("\\.");
		Random r = new Random();
		int a = r.nextInt(256);
		int b = r.nextInt(256);
		int c = r.nextInt(254) + 1;
		switch (size) {
		case A:
			result = bytes[0] + "." + a + "." + b + "." + c;
			break;

		case B:
			result = bytes[0] + "." + bytes[1] + "." + b + "." + c;
			break;

		case C:
			result = bytes[0] + "." + bytes[1] + "." + bytes[2] + "." + c;
			break;
		}
		return result;
	}

	public void decreaseHitRate() {
		this.hit_rate = this.hit_rate / 2;
	}
}
