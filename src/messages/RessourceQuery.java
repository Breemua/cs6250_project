package messages;

import chord.ChordKey;

public class RessourceQuery extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6874931436116294243L;

	private ChordKey key;

	public RessourceQuery(String senderId, ChordKey key) {
		super(senderId);
		this.setKey(key);
	}

	public ChordKey getKey() {
		return key;
	}

	public void setKey(ChordKey key) {
		this.key = key;
	}

}
