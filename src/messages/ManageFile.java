package messages;

import java.math.BigInteger;

public class ManageFile extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4254110849213959715L;

	public enum Type {ADD, REMOVE};
	
	private BigInteger key;
	private Type type;
	
	public ManageFile(BigInteger key, String senderId, Type type) {
		super(senderId);
		this.key = key;
		this.type = type;
	}

	public BigInteger getKey() {
		return key;
	}
	
	public Type getType() {
		return type;
	}

}
