package messages;

import java.io.Serializable;

import common.Log;

public class Message implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8288864345848337466L;

	private String senderId;

	public Message(String senderId) {
		this.senderId = senderId;
	}

	public void printToto() {
		Log.getLogger().info("Toto");
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

}
