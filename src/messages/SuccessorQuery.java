package messages;

import chord.ChordKey;

public class SuccessorQuery extends StandardQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9178858312662708916L;

	public SuccessorQuery(String senderId, ChordKey key) {
		super(senderId, key);
	}

}
