package messages;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;

public class ExitNotification extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5300783829234240940L;
	private String predecessorId;
	private String successorId;
	private Map<BigInteger, Set<String>> responsabilities;

	public ExitNotification(String senderId, 
			String predecessorId, 
			String successorId,
			Map<BigInteger, Set<String>> responsabilities) {
		super(senderId);
		
		this.predecessorId = predecessorId;
		this.successorId = successorId;
		this.responsabilities = responsabilities;
	}

	public String getPredecessorId() {
		return predecessorId;
	}

	public String getSuccessorId() {
		return successorId;
	}

	public Map<BigInteger, Set<String>> getResponsabilities() {
		return responsabilities;
	}

}
