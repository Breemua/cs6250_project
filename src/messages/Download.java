package messages;

public class Download extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3111784579043233158L;
	private String fileName;

	public Download(String senderId, String fileName) {
		super(senderId);
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

}
