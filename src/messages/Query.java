package messages;

import java.math.BigInteger;

import common.Hash;

public class Query extends Message{

	public static enum Type {RESPONSIBLE, OWNERS};
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8531710460492767404L;
	public BigInteger key;
	private Type type;
	
	public Query(BigInteger key, String senderID, Type type) {
		super(senderID);
		this.key = key;
		this.type = type;
	}
	
	public Query(String file, String senderID) {
		super(senderID);
		this.key = new BigInteger(Hash.hashFile(file));
		this.type = Type.RESPONSIBLE;
	}
	
	public Type getType() {
		return this.type;
	}
	
}
