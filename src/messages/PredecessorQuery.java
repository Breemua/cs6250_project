package messages;

public class PredecessorQuery extends StandardQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2402915467927575110L;

	public PredecessorQuery(String senderId) {
		super(senderId, null);
	}

}
