package messages;

import chord.ChordKey;

public class PredecessorNotification extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5218754653533592588L;
	private ChordKey key;

	public PredecessorNotification(String senderId, ChordKey key) {
		super(senderId);
		this.key = key;
	}

	public ChordKey getKey() {
		return key;
	}

	public void setKey(ChordKey key) {
		this.key = key;
	}

}
