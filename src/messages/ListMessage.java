package messages;

import java.util.Vector;

public class ListMessage extends Message{

	/**
	 * 
	 */
	private static final long serialVersionUID = 118566476661513510L;
	private Vector<String> IPs;
	
	public ListMessage(String senderId,Vector<String> ips) {
		super(senderId);
		IPs=ips;
	}
	


	public Vector<String> getIPs(){
		return IPs;
	}

}
