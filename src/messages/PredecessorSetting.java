package messages;

import chord.ChordKey;

public class PredecessorSetting extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6825842684996619516L;
	private ChordKey predecessorToSet;

	public PredecessorSetting(String senderId, ChordKey predecessorToSet) {
		super(senderId);
		// TODO Auto-generated constructor stub
		this.predecessorToSet = predecessorToSet;
	}

	public ChordKey getPredecessorToSet() {
		return predecessorToSet;
	}

	public void setPredecessorToSet(ChordKey predecessorToSet) {
		this.predecessorToSet = predecessorToSet;
	}

}
