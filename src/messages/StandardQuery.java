package messages;

import chord.ChordKey;

public class StandardQuery extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1601652864172286407L;

	private ChordKey key;

	public StandardQuery(String senderId, ChordKey key) {
		super(senderId);
		this.key = key;
	}

	public ChordKey getKey() {
		return key;
	}

	public void setKey(ChordKey key) {
		this.key = key;
	}

}
