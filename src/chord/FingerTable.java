package chord;

import common.Hash;

public class FingerTable {

	Finger[] fingers;

	public FingerTable(ChordKey key) {
		this.fingers = new Finger[Hash.KEY_LENGTH];
		for (int i = 0; i < fingers.length; i++) {
			ChordKey start = key.createStartKey(i);
			fingers[i] = new Finger(start, key);
		}
	}

	public Finger getFinger(int i) {
		return fingers[i];
	}

}
