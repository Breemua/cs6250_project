package chord;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import messages.Download;
import messages.ExitNotification;
import messages.ManageFile;
import messages.Null;
import messages.PredecessorNotification;
import messages.PredecessorQuery;
import messages.PredecessorSetting;
import messages.Query;
import messages.StabilizationMessage;
import messages.SuccessorQuery;

import common.Log;

public class ChordServer implements Runnable {

	private ChordNode node;
	private Socket fromClientSocket;
	private Map<String,String> seededFiles;

	public ChordServer(Socket socket, ChordNode node, Map<String,String> seededFiles) {
		this.fromClientSocket = socket;
		this.node = node;
		this.seededFiles = seededFiles;
	}

	@Override
	public void run() {

		try {
			ObjectOutputStream oos = new ObjectOutputStream(
					fromClientSocket.getOutputStream());

			ObjectInputStream ois = new ObjectInputStream(
					fromClientSocket.getInputStream());

			Object msg = (Object) ois.readObject();

			if (msg instanceof SuccessorQuery) {
				Log.getLogger().info("SuccessorQuery");
				oos.writeObject(node.findSuccessor(((SuccessorQuery) msg)
						.getKey()));
			} else if (msg instanceof PredecessorQuery) {
				Log.getLogger().info("PredecessorQuery");
				if (node.predecessor == null) {
					Log.getLogger().info("null");
					oos.writeObject(new Null(null));
				} else {
					Log.getLogger().info(node.predecessor.getIdentifier());
					oos.writeObject(node.predecessor);
				}
			} else if (msg instanceof StabilizationMessage) {
				Log.getLogger().info("StabilizationMessage");
				node.stabilize();
				oos.writeObject(new Null(null));
			} else if (msg instanceof PredecessorSetting) {
				System.out
				.println("Set predecessor to "
						+ ((PredecessorSetting) msg)
						.getPredecessorToSet().getIdentifier());
				node.setPredecessor(((PredecessorSetting) msg)
						.getPredecessorToSet());
				oos.writeObject(new Null(null));
			} else if (msg instanceof PredecessorNotification) {
				Log.getLogger().info("Predecessor notification");
				oos.writeObject(
						node.notifyPredecessor(((PredecessorNotification) msg)
								.getKey()));

			} else if (msg instanceof Query) {
				if(((Query) msg).getType() == Query.Type.OWNERS) {
					if(node.responsability.containsKey(((Query) msg).key)) {
						oos.writeObject(node.responsability.get(((Query) msg).key));
					} else {
						oos.writeObject(new Null(null));
					}
				} else if (((Query) msg).getType() == Query.Type.RESPONSIBLE) {
					oos.writeObject(
							node.findSuccessor(new ChordKey(((Query) msg).key))
							.getOwnersOf(((Query) msg).key, node.getNodeKey()));
				}
			} else if (msg instanceof ManageFile){
				ManageFile manageFile = (ManageFile) msg;
				if(manageFile.getType() == ManageFile.Type.ADD) {
					if(node.responsability.containsKey(manageFile.getKey())) {
						node.responsability.get(manageFile.getKey()).add(manageFile.getSenderId());
					} else {
						Set<String> set = new HashSet<String>();
						set.add(manageFile.getSenderId());
						node.responsability.put(manageFile.getKey(), set);
					}
				} else if(manageFile.getType() == ManageFile.Type.REMOVE) {
					if(node.responsability.containsKey(manageFile.getKey())) {
						node.responsability.get(manageFile.getKey()).remove(manageFile.getSenderId());
					}
				}
				oos.writeObject(new Null(null));
			} else if (msg instanceof Download) {
				byte[] result = getFile(((Download) msg).getFileName());
				if(result != null) {
					oos.writeObject(result);
				} else {
					oos.writeObject(new Null(null));
				}
			} else if (msg instanceof ExitNotification){
				ExitNotification exitNotif = (ExitNotification) msg;
				if(node.getPredecessor().getIdentifier().equals(exitNotif.getSenderId())) {
					node.setPredecessor(new ChordKey(exitNotif.getPredecessorId()));
					node.responsability.putAll(exitNotif.getResponsabilities());
				} else if (node.getSuccessor().getIdentifier().equals(exitNotif.getSenderId())) {
					node.setSuccessor(new ChordKey(exitNotif.getSuccessorId()));
				} 
				oos.writeObject(new Null(null));
				
			} else {
				Log.getLogger().info("unknown message");
			}

			Log.getLogger().info("Server");
			Log.getLogger().info(node.toString());

			oos.close();
			ois.close();
			fromClientSocket.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private byte[] getFile(String fileName) {
		byte[] yourBytes = null;
		try {
			String filePath = this.seededFiles.get(fileName);
			if(filePath==null) {
				Log.getLogger().info("File "+fileName+" was not seeded on this computer");
			} else {
				final File fileToSend = new File(filePath);
				final BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileToSend));
				yourBytes = new byte[(int)fileToSend.length()];
				bis.read(yourBytes);
				for(int i=0; i<20;i++) {
					System.out.print(yourBytes[i]);
				}
				System.out.println();
				bis.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return yourBytes;
	}

}