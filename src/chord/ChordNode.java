package chord;

import java.io.PrintStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import bootstrap.Bootstrapper;

import common.Hash;
import common.Log;

public class ChordNode implements Runnable {

	ChordKey nodeKey;

	ChordKey predecessor;

	ChordKey successor;

	FingerTable fingerTable;

	String nextNode;


	Map<BigInteger, Set<String>> responsability;

	public ChordNode(String nodeId) {
		Log.getLogger().info("Next node is : " + nextNode);
		this.nodeKey = new ChordKey(nodeId);
		this.fingerTable = new FingerTable(this.nodeKey);
		this.responsability = new HashMap<BigInteger, Set<String>>();
		this.create();
	}

	@Override
	public void run() {

		try {
			synchronized (this) {
				this.wait();
				this.nextNode=Bootstrapper.getFirstNode();
			}
			
			
			if (this.nextNode != null && 
					(this.predecessor == null || this.predecessor.equals(this.nodeKey))) {
				String tmp[] = this.nextNode.split(":");
				join(new ChordKey(new URL("http", tmp[0], 9000, "").toString()));
				Log.getLogger().info(toString());

				ChordKey preceding = getSuccessor().getPredecessor();
				stabilize();

				if (preceding == null) {
					Log.getLogger().info("preceding is null");
					getSuccessor().stabilize();
				} else {
					if (preceding.equals(getNodeKey())) {
						stabilize();
					} else {
						preceding.stabilize();
					}
				}

				Log.getLogger().info("Initialization done " + toString());
			} else {
				Log.getLogger().info(
						"Initialization done without bootstrapping info");
			}

			while (true) {

				try {
					Thread.sleep(60000);
					stabilize();
					fixFingers();
					printFingerTable(System.out);
					// Log.getLogger().info("trolol " + ((Set<String>)
					// Network.communicate(
					// nodeKey.getIdentifier(),
					// new Query(new BigInteger("132"), null,
					// Query.Type.RESPONSIBLE))).toArray()[0]);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Map<BigInteger, Set<String>> notifyPredecessor(ChordKey key) {
		Map<BigInteger, Set<String>> result = new HashMap<BigInteger, Set<String>>();
		if (predecessor == null
				|| key.isBetween(predecessor, this.getNodeKey())) {
			if (predecessor == null) {
				predecessor = this.getNodeKey();
			}
			for (BigInteger i : this.responsability.keySet()) {
				if (new ChordKey(i).isBetween(predecessor, key)) {
					result.put(i, this.responsability.get(i));
				}
			}
			for (BigInteger i : result.keySet()) {
				this.responsability.remove(i);
			}
			predecessor = key;

		}

		return result;
	}

	/**
	 * Lookup a successor of given identifier
	 * 
	 * @param identifier
	 *            an identifier to lookup
	 * @return the successor of given identifier
	 */
	public ChordKey findSuccessor(String identifier) {
		ChordKey key = new ChordKey(identifier);
		return findSuccessor(key);
	}

	/**
	 * Lookup a successor of given key
	 * 
	 * @param identifier
	 *            an identifier to lookup
	 * @return the successor of given identifier
	 */
	public ChordKey findSuccessor(ChordKey key) {

		if (this.nodeKey.equals(successor)) {
			return this.nodeKey;
		}

		if (key.isBetween(this.getNodeKey(), successor)
				|| key.compareTo(successor) == 0) {
			return successor;
		} else {
			ChordKey nodeKey = closestPrecedingNode(key);
			if (nodeKey == this.nodeKey) {
				return successor.findSuccessor(this, key);
			}
			return nodeKey.findSuccessor(this, key);
		}
	}

	public ChordKey closestPrecedingNode(ChordKey key) {
		for (int i = Hash.KEY_LENGTH - 1; i >= 0; i--) {
			Finger finger = fingerTable.getFinger(i);
			ChordKey fingerKey = finger.getKey();
			if (fingerKey.isBetween(this.getNodeKey(), key)) {
				Log.getLogger().info(
						fingerKey + " is between " + this.getNodeKey()
						+ " and " + key);
				return finger.getKey();
			}
			Log.getLogger().info(
					fingerKey + " is not between " + this.getNodeKey()
					+ " and " + key);
		}
		return this.nodeKey;
	}

	/**
	 * Creates a new Chord ring.
	 */
	public void create() {
		predecessor = null;
		successor = this.nodeKey;
	}

	/**
	 * Joins a Chord ring with a node in the Chord ring
	 * 
	 * @param node
	 *            a bootstrapping node
	 */
	public void join(ChordKey node) {
		predecessor = null;
		successor = node.findSuccessor(this, this.nodeKey);

	}

	/**
	 * Verifies the successor, and tells the successor about this node. Should
	 * be called periodically.
	 */
	public void stabilize() {
		ChordKey nodeKey = successor.getPredecessor(this);
		if (nodeKey != null) {

			if ((this.nodeKey == successor)
					|| nodeKey.isBetween(this.getNodeKey(), successor)) {
				successor = nodeKey;
			}
		}

		@SuppressWarnings("unchecked")
		Map<BigInteger, Set<String>> newResponsability = (Map<BigInteger, Set<String>>) successor
		.notifyPredecessor(this.getNodeKey());
		if(newResponsability != null) {
			this.responsability.putAll(newResponsability);
		}

	}

	/**
	 * Refreshes finger table entries.
	 */
	public void fixFingers() {
		for (int i = 0; i < Hash.KEY_LENGTH; i++) {
			Finger finger = fingerTable.getFinger(i);
			ChordKey key = finger.getStart();
			finger.setKey(findSuccessor(key));
		}
	}

	public void disconnect() {
		if(this.predecessor!=null) {
			this.predecessor.notifyDisconnection(this);
		}
		if(!this.successor.equals(this.nodeKey)) {
			this.successor.notifyDisconnection(this);
		}
		this.predecessor = null;
		this.successor = null;	
		Log.getLogger().info("Disconnecting the chord Node");
	}

	public Set<String> getOwnersOf(BigInteger key) {
		ChordKey clef  = findSuccessor(new ChordKey(key));
		Log.getLogger().info("KEY OWNER : "+clef.getIdentifier());
		return (Set<String>) clef.getOwnersOf(key, this.getNodeKey());
	}

	public void addFile(BigInteger key) {
		this.findSuccessor(new ChordKey(key)).addFile(key,
				this.nodeKey.getIdentifier());
	}

	public void removeFile(BigInteger key) {
		this.findSuccessor(new ChordKey(key)).removeFile(key,
				this.nodeKey.getIdentifier());
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ChordNode[  " + "\n");
		sb.append("Id          " + this.nodeKey.getIdentifier() + "\n");
		if (this.predecessor == null) {
			sb.append("Predecessor is null" + "\n");
		} else {
			sb.append("Predecessor " + this.predecessor.getIdentifier() + "\n");
		}
		sb.append("Successor   " + this.successor.getIdentifier() + "\n" + "\n");
		return sb.toString();
	}

	public void printFingerTable(PrintStream out) {
		out.println("=======================================================");
		out.println("FingerTable: " + this);
		out.println("-------------------------------------------------------");
		out.println("Predecessor: " + predecessor);
		out.println("Successor: " + successor);
		out.println("-------------------------------------------------------");
		for (int i = 0; i < Hash.KEY_LENGTH; i++) {
			Finger finger = fingerTable.getFinger(i);
			out.println(finger.getStart() + "\t"
					+ finger.getKey().getIdentifier());
		}
		out.println("=======================================================");
	}

	public ChordKey getNodeKey() {
		return nodeKey;
	}

	public void setNodeKey(ChordKey nodeKey) {
		this.nodeKey = nodeKey;
	}

	public ChordKey getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(ChordKey predecessor) {
		this.predecessor = predecessor;
	}

	public ChordKey getSuccessor() {
		return successor;
	}

	public void setSuccessor(ChordKey successor) {
		this.successor = successor;
	}

	public FingerTable getFingerTable() {
		return fingerTable;
	}

	public void setFingerTable(FingerTable fingerTable) {
		this.fingerTable = fingerTable;
	}

}
