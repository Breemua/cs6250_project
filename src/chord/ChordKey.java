package chord;

import java.io.Serializable;
import java.math.BigInteger;

import messages.ExitNotification;
import messages.ManageFile;
import messages.Null;
import messages.PredecessorNotification;
import messages.PredecessorQuery;
import messages.PredecessorSetting;
import messages.Query;
import messages.StabilizationMessage;
import messages.SuccessorQuery;

import common.Hash;
import common.Network;

public class ChordKey implements Comparable<Object>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5444408195033883296L;

	private String identifier;

	private BigInteger key;

	public ChordKey(BigInteger key) {
		this.key = key;
	}

	public ChordKey(String identifier) {
		this.identifier = identifier;
		this.key = new BigInteger(Hash.hash(identifier));
	}
	
	public boolean isBetween(ChordKey fromKey, ChordKey toKey) {
		if (fromKey.compareTo(toKey) < 0) {
			if (this.compareTo(fromKey) > 0 && this.compareTo(toKey) < 0) {
				return true;
			}
		} else if (fromKey.compareTo(toKey) > 0) {
			if (this.compareTo(toKey) < 0 || this.compareTo(fromKey) > 0) {
				return true;
			}
		}
		return false;
	}

	public ChordKey createStartKey(int index) {
		BigInteger i = new BigInteger(((Integer)index).toString());
		return new ChordKey(this.key.
				add(new BigInteger("2").
						modPow(i, Hash.MAX_VALUE)).
						mod(Hash.MAX_VALUE));
	}

	public int compareTo(Object obj) {
		return this.key.compareTo(((ChordKey) obj).key);
	}

	@Override
	public boolean equals(Object obj) {
		return this.compareTo(obj) == 0;
	}
	
	public String toString() {
		return this.key.toString();
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public BigInteger getKey() {
		return key;
	}

	public ChordKey findSuccessor(ChordNode chordNode, ChordKey key) {

		return (ChordKey) Network.communicate(this.identifier,
				new SuccessorQuery(chordNode.nodeKey.getIdentifier(), key));
	}
	
	public ChordKey getPredecessor(ChordNode sender) {

		Object result = Network.communicate(this.identifier,
				new PredecessorQuery(sender.nodeKey.getIdentifier()));

		if (result instanceof Null) {
			return null;
		} else {
			return (ChordKey) result;
		}
	}

	public ChordKey getPredecessor() {
		Object result = Network.communicate(this.identifier,
				new PredecessorQuery(null));
		if (result instanceof Null) {
			return null;
		} else {
			return (ChordKey) result;
		}
	}

	public void stabilize() {
		Network.communicate(this.identifier,
				new StabilizationMessage(this.getIdentifier()));
	}

	public void setPredecessor(ChordKey nodeKey) {
		Network.communicate(this.identifier,
				new PredecessorSetting(this.getIdentifier(), nodeKey));
	}

	public Object notifyPredecessor(ChordKey nodeKey) {
		return Network.communicate(this.identifier,
				new PredecessorNotification(this.getIdentifier(), nodeKey));
	}

	public Object getOwnersOf(BigInteger key, ChordKey nodeKey) {
		return Network.communicate(this.identifier,
				new Query(key, nodeKey.getIdentifier(), Query.Type.OWNERS));
	}
	
	public void addFile(BigInteger key, String sourceID) {
		Network.communicate(this.identifier, 
				new ManageFile(key, sourceID, ManageFile.Type.ADD));
	}
	
	public void removeFile(BigInteger key, String sourceID) {
		Network.communicate(this.identifier, 
				new ManageFile(key, sourceID, ManageFile.Type.REMOVE));
	}

	public void notifyDisconnection(ChordNode chordNode) {
		Network.communicate(this.identifier,
				new ExitNotification(chordNode.getNodeKey().getIdentifier(),
						chordNode.getPredecessor().getIdentifier(), 
						chordNode.getSuccessor().getIdentifier(),
						chordNode.responsability));
	}

}
