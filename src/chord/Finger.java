package chord;

public class Finger {

	private ChordKey start;

	private ChordKey key;

	public Finger(ChordKey start, ChordKey key) {
		this.key = key;
		this.start = start;
	}

	/**
	 * @return the start
	 */
	public ChordKey getStart() {
		return start;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(ChordKey start) {
		this.start = start;
	}

	/**
	 * @return the key
	 */
	public ChordKey getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(ChordKey key) {
		this.key = key;
	}

}
