package chord;

public class ChordException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1553953575926870984L;

	public ChordException() {
		super();
	}

	public ChordException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChordException(String message) {
		super(message);
	}

	public ChordException(Throwable cause) {
		super(cause);
	}

}
