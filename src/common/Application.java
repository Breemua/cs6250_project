package common;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.BindException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import messages.Download;
import bootstrap.BootServer;
import bootstrap.Bootstrapper;
import bootstrap.HitList;
import chord.ChordNode;
import chord.ChordServer;


public class Application extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JButton seedFileButton=null;
	private JButton leechFileButton=null;
	private JTextArea logArea=null;
	private ChordNode chordNode=null;
	private JComboBox seedComboBox = null;
	private Map<String,BigInteger> availableFiles;
	private BigInteger currentSeedHash;
	private Map<String,String> seededFiles;

	private class ExitListener extends WindowAdapter {
		
		private ChordNode chordNode;
		
		public ExitListener() {
			super();
			chordNode=null;
		}
		
		public void setChordNode(ChordNode chordNode) {
			this.chordNode=chordNode;
		}
		
		@Override
		public void windowClosing(WindowEvent ev) {
			if(this.chordNode!=null) {
				chordNode.disconnect();
				HitList.exit(); 
			}
		}
	}
	private ExitListener exitListener;
	
	public Application() {
		this.setTitle("CS6250 Final Project");
		this.setSize(500,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.seededFiles = new HashMap<String,String>();
		
		exitListener = new ExitListener();
		this.addWindowListener(exitListener);
		
		availableFiles = new HashMap<String,BigInteger>();
		this.getAvailableFiles();
		
		seedFileButton = new JButton("Seed a File");
		seedFileButton.addActionListener(this);

		leechFileButton = new JButton("Retrieve a File");
		leechFileButton.addActionListener(this);

		logArea = new JTextArea(10,40);
		logArea.setEditable(false);
		JScrollPane logScrollPane = new JScrollPane(logArea);

		TextAreaHandler logAreaHandler = new TextAreaHandler(logArea);
		Log.getLogger().addHandler(logAreaHandler);

		this.seedComboBox = new JComboBox(new Vector<String>(availableFiles.keySet()));
		seedComboBox.addActionListener(this);

		JPanel pan = new JPanel();
		pan.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		gbc.weighty=0.2;

		pan.add(seedFileButton,gbc);

		gbc.gridy=1;
		gbc.gridwidth=1;
		pan.add(leechFileButton,gbc);

		gbc.gridy=1;
		gbc.gridx=1;
		gbc.gridwidth=4;
		pan.add(seedComboBox,gbc);

		gbc.gridy=2;
		gbc.gridx=0;
		gbc.gridheight=GridBagConstraints.REMAINDER;
		gbc.gridwidth=GridBagConstraints.REMAINDER;
		gbc.weighty=0.4;
		gbc.anchor = GridBagConstraints.PAGE_END;
		pan.add(logScrollPane,gbc);

		this.setContentPane(pan);
		this.setVisible(true);
	}

	public Bootstrapper startBootstrapper() {
		Bootstrapper b = new Bootstrapper();
		BootServer bs = new BootServer();
		(new Thread(bs)).start();
		(new Thread(b)).start();
		return b;
	}

	public ChordNode startChordNode(Bootstrapper bs) {
		String host="";
		int port = 9000;
		try {
			Enumeration<NetworkInterface> e = NetworkInterface
					.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				NetworkInterface n = e.nextElement();
				Enumeration<InetAddress> ee = n.getInetAddresses();
				while (ee.hasMoreElements()) {
					InetAddress i = ee.nextElement();
					if (!i.getHostAddress().toString().substring(0, 5).equals("127.0") && i instanceof Inet4Address) {
						host=i.getHostAddress().toString();
					}
				}
			}

			URL url = new URL("http", host, port, "");
			Log.getLogger().info("Local address is : "+url.toString());

			this.chordNode = new ChordNode(url.toString());
			exitListener.setChordNode(this.chordNode);
			new Thread(this.chordNode).start();

			Log.getLogger().info("Added new CHORD node : "+this.chordNode.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return this.chordNode;
	}

	public void startChordServer(int port, ChordNode node) {
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			Log.getLogger().info("Waiting for a connection on " + port);

			while (true) {
				Socket fromClientSocket = serverSocket.accept();
				Runnable connectionHandler = new ChordServer(fromClientSocket,
						node,this.seededFiles);
				(new Thread(connectionHandler)).start();

			}
		} catch(BindException e1) {
			Log.getLogger().severe("Port "+port+" already in use");
		}catch (IOException e) {
			e.printStackTrace();
		} 

	}

	public void getAvailableFiles() {
		FileInputStream fis;
		try {
			fis = new FileInputStream("src/SeedFiles.txt");
			DataInputStream in = new DataInputStream(fis);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String current_line;
			while( (current_line=br.readLine()) != null) {
				String[] tmp = current_line.split(" ");
				availableFiles.put(tmp[1], new BigInteger(tmp[0]));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==seedFileButton) {

			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				this.seededFiles.put(file.getName(), file.getAbsolutePath());
				
				Log.getLogger().info("Opened new file to seed : "+file.getAbsolutePath());

				byte[] fileHash = Hash.hashFile(file.getAbsolutePath());
				BigInteger integerHash = new BigInteger(fileHash);
				if(this.chordNode!=null) {
					this.chordNode.addFile(integerHash);
					Log.getLogger().info("Added hash "+integerHash.toString()+" of file "+file.getAbsolutePath()+" to the Chord Node.");
				}

			} 

		} else if(e.getSource()==leechFileButton) {
			Log.getLogger().info("Leeching file hash : "+currentSeedHash+".\nOwners are : ");
			long time = System.currentTimeMillis();
			Set<String> owners = this.chordNode.getOwnersOf(currentSeedHash);
			System.out.println("LEECHLEECHLEECH "+(System.currentTimeMillis()-time));
			for(String s : owners) {
				Log.getLogger().info(s);
			}

			Object result = Network.communicate((String) owners.toArray()[0], new Download(null, (String) this.seedComboBox.getSelectedItem()));

			if(result instanceof byte[]) {
				try {
					final File file = new File((String) this.seedComboBox.getSelectedItem());
					final BufferedOutputStream bis = new BufferedOutputStream(new FileOutputStream(file));
					bis.write((byte[]) result);
					bis.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

		} else if(e.getSource()==this.seedComboBox) {
			this.currentSeedHash = availableFiles.get(this.seedComboBox.getSelectedItem());
		}

	}

    public static void main(String[] args) {
	    Application ex = new Application();
	    ex.setVisible(true);
	    Bootstrapper bs = ex.startBootstrapper();
	    ChordNode node = ex.startChordNode(bs);
	    bs.setChord(node);
	    ex.startChordServer(9000, node);
    }
}

