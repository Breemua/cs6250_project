package common;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	private static Logger logger;


	private static void init() {
		logger = Logger.getLogger("MyLog");
		FileHandler fh;

		try {

			fh = new FileHandler("./src/LogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static Logger getLogger() {
		if (logger == null) {
			init();
		}
		return logger;
	}
	
	public static void addHandler(Handler h) {
		Logger l = getLogger();
		l.addHandler(h);
	}

}
