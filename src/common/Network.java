package common;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import messages.Message;

public class Network {

	public static Object communicate(String ip_port, Message message) {
		Socket socket;
		String[] split = ip_port.split(":+|/+");
		String ip = split[split.length - 2];
		String portNumber = split[split.length - 1];
		Object result = null;

		Log.getLogger().info("sending message "
				+ message.getClass().getSimpleName() + " to " + ip_port);
		
//		if(ip_port == message.getSenderId()) {
//			System.out.println("talking to youself ?");
//		} else {

		try {
			socket = new Socket(ip, Integer.parseInt(portNumber));

			ObjectInputStream ois = new ObjectInputStream(
					socket.getInputStream());

			ObjectOutputStream oos = new ObjectOutputStream(
					socket.getOutputStream());

			oos.writeObject(message);

			result = ois.readObject();

			ois.close();
			oos.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
//		}
		return result;

	}

}
