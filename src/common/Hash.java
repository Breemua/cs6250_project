package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Hash {

	public static String function = "Java";

	public static int KEY_LENGTH = 8;

	public static BigInteger MAX_VALUE = new BigInteger("2").pow(KEY_LENGTH);

	public static byte[] hashFile(String file) {

		try {
			File f = new File(file);
			FileInputStream fis = new FileInputStream(f);
			byte[] buffer = new byte[1024];
			int bytesRead;

			MessageDigest md;
			md = MessageDigest.getInstance("SHA-256");
			md.reset();
			while((bytesRead = fis.read(buffer)) != -1) {
				md.update(buffer, 0, bytesRead);
			}
			fis.close();
			byte[] digest = md.digest();
			byte[] res = Arrays.copyOfRange(digest, 0, KEY_LENGTH/8 + 1);
			res[0] = 0x00;
			return res;
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] hash(String identifier) {

		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.reset();
			byte[] res = Arrays.copyOfRange(md.digest(identifier.getBytes()), 0, KEY_LENGTH/8 + 1);
			res[0] = 0x00;
			return res;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;

		//		if (function.equals("SHA-1")) {
		//			try {
		//				MessageDigest md = MessageDigest.getInstance(function);
		//				md.reset();
		//				byte[] code = md.digest(identifier.getBytes());
		//				byte[] value = new byte[KEY_LENGTH / 8];
		//				int shrink = code.length / value.length;
		//				int bitCount = 1;
		//				for (int j = 0; j < code.length * 8; j++) {
		//					int currBit = ((code[j / 8] & (1 << (j % 8))) >> j % 8);
		//					if (currBit == 1)
		//						bitCount++;
		//					if (((j + 1) % shrink) == 0) {
		//						int shrinkBit = (bitCount % 2 == 0) ? 0 : 1;
		//						value[j / shrink / 8] |= (shrinkBit << ((j / shrink) % 8));
		//						bitCount = 1;
		//					}
		//				}
		//				return value;
		//			} catch (Exception e) {
		//				e.printStackTrace();
		//			}
		//		}
		//
		//		if (function.equals("CRC32")) {
		//			CRC32 crc32 = new CRC32();
		//			crc32.reset();
		//			crc32.update(identifier.getBytes());
		//			long code = crc32.getValue();
		//			code &= (0xffffffffffffffffL >>> (64 - KEY_LENGTH));
		//			byte[] value = new byte[KEY_LENGTH / 8];
		//			for (int i = 0; i < value.length; i++) {
		//				value[value.length - i - 1] = (byte) ((code >> 8 * i) & 0xff);
		//			}
		//			return value;
		//		}
		//
		//		if (function.equals("Java")) {
		//			int code = identifier.hashCode();
		//			code &= (0xffffffff >>> (32 - KEY_LENGTH));
		//			byte[] value = new byte[KEY_LENGTH / 8];
		//			for (int i = 0; i < value.length; i++) {
		//				value[value.length - i - 1] = (byte) ((code >> 8 * i) & 0xff);
		//			}
		//			return value;
		//		}
		//
		//		return null;
	}

	public static String getFunction() {
		return function;
	}

	public static void setFunction(String function) {
		if (function.equals("SHA-1")) {
			Hash.KEY_LENGTH = 160;
		}
		if (function.equals("CRC32")) {
			Hash.KEY_LENGTH = 64;
		}
		if (function.equals("Java")) {
			Hash.KEY_LENGTH = 32;
		}
		Hash.function = function;
	}

	public static int getKeyLength() {
		return KEY_LENGTH;
	}

	public static void setKeyLength(int keyLength) {
		KEY_LENGTH = keyLength;
	}

}
