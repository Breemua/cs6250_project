package common;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class TextAreaHandler extends Handler {

	private JTextArea logArea;
	
	public TextAreaHandler(JTextArea area) {
		super();
		this.logArea=area;
	}


	@Override
	public void publish(final LogRecord arg0) {
		
		 SwingUtilities.invokeLater(new Runnable() {

	            @Override
	            public void run() {
	                logArea.append(arg0.getMessage());
	                logArea.append("\n");
	            }

	        });
	}
	
	@Override
	public void close() {
		
	}
	
	@Override
	public void flush() {
	}

}
